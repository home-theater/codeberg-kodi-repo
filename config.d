module config;

import common : GitPath;

string repo_addon_id = "repository.home-theater-codeberg";

GitPath[] regular_addon_git_paths = [
  GitPath("https://codeberg.org/home-theater/codeberg-kodi-repo_addon", "/repository.home-theater-codeberg/"),
  GitPath("https://codeberg.org/home-theater/kodi-addon_remote-config", "/script.remote-config/"),
  GitPath("https://github.com/peno64/service.subtitles.opensubtitles_by_opensubtitles_dualsub")
];

string[] regular_addon_zip_urls = [
  "https://tikipeter.github.io/repository.tikipeter-1.0.0.zip"
];
